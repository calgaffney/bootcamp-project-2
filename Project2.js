const express = require('express');
const path = require('path');
var cookieParser = require('cookie-parser');
const app = express();
const port = 5500;
app.use(express.json());
app.use(express.urlencoded());
app.use(cookieParser())
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(express.static(__dirname + '/'));

app.get('/', (req, res) => {
    console.log(req.body)
    res.writeContinue(req.body)
    });
    
app.post('/', (req, res) => {
    console.log(req.body, req.ip, req.headers.referer, req.cookies);
    res.send(`${JSON.stringify(req.body)},\n${req.ip},\n${req.headers.referer},\n${JSON.stringify(req.cookies)}`);
})

app.listen(port, () => {
    console.log(`Project2 listening on port ${port}`)
})

app.use("/css", express.static(path.join(__dirname, "node_modules/bootstrap/dist/css")))
app.use("/js", express.static(path.join(__dirname, "node_modules/bootstrap/dist/js")))